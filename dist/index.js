let CLIParameterParser = require('./lib/cliParameterParser').CLIParameterParser;
let LispValidator = require('./lib/lispValidator').LispValidator;

if(process.argv.length === 3 && process.argv[2].trim() && process.argv[2].trim() != '')
{
    const input = process.argv[2].trim();
    let cliParameterParser = new CLIParameterParser();
    let lispValidator = new LispValidator();

    // console.log(`input: ${input}`)

    cliParameterParser.parse(input).then((data) => {

        console.log(lispValidator.isValid(data));
    });
}
else
    console.log('Missing input parameter: string || file path. Input strings containing spaces should be wrapped in double quotes.')