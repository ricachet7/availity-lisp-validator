"use strict";
exports.__esModule = true;
exports.LispValidator = void 0;
var getParenthenses = function (lispData) {
    var regEx = /(?<opening>\()|(?<closing>\))/g;
    return lispData.matchAll(regEx);
};
var getCount = function (lispData, groupName) {
    var c = Array.from(getParenthenses(lispData), function (m) { return m.groups[groupName]; }).filter(function (m) { return m != undefined; });
    return c.length;
};
var _opening = 'opening';
var countOpenings = function (lispData) { return getCount(lispData, _opening); };
var _closing = 'closing';
var countClosings = function (lispData) { return getCount(lispData, _closing); };
var LispValidator = (function () {
    function LispValidator() {
    }
    LispValidator.prototype.isValid = function (lispData) {
        var data = lispData.trim();
        var numOfOpenings = countOpenings(data);
        var numOfClosings = countClosings(data);
        return numOfOpenings === numOfClosings
            && data[0] === '('
            && data[data.length - 1] === ')';
    };
    return LispValidator;
}());
exports.LispValidator = LispValidator;
//# sourceMappingURL=lispValidator.js.map