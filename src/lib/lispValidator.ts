const getParenthenses = (lispData: string): IterableIterator<RegExpMatchArray> => {

    let regEx:RegExp = /(?<opening>\()|(?<closing>\))/g;

    return lispData.matchAll(regEx);
}

const getCount = (lispData: string, groupName: string): number => {

    let c:string[] = Array.from(getParenthenses(lispData), m => m.groups[groupName]).filter( m => m != undefined);

    return c.length;
}

const _opening: string = 'opening';

const countOpenings = (lispData: string): number => getCount(lispData, _opening)

const _closing: string = 'closing';

const countClosings = (lispData: string): number => getCount(lispData, _closing)

export class LispValidator{

    public isValid(lispData: string): boolean {

        const data = lispData.trim();
        const numOfOpenings: number = countOpenings(data);
        const numOfClosings: number =  countClosings(data);

        //console.log(`numOfOpenings: ${numOfOpenings}`)
        //console.log(`numOfClosings: ${numOfClosings}`)

        return numOfOpenings === numOfClosings 
            && data[0] === '(' 
            && data[data.length - 1] === ')';
    }
}