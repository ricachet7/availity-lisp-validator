
import { promises as fs } from "fs";
import { constants } from 'fs';

const extractFromFile = async (filePath: string): Promise<string> => {

    try {
        await fs.access(filePath, constants.R_OK);
        const contentBuf = await fs.readFile(filePath);

        return contentBuf.toString()

      } catch {
      }

      return null;
}

export class CLIParameterParser{
    public async parse(data: string): Promise<string> {

        const dataFromFile = await extractFromFile(data);
        
        if(dataFromFile) return dataFromFile;

        return data;
    }
}