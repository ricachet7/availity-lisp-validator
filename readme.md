Description
======================
You are tasked to write a checker that validates the parentheses of a LISP code.  Write a program (in Java or JavaScript) which takes in a string as an input and returns true if all the parentheses in the string are properly closed and nested.

Dependencies
=======================
Node.js Version v16.13.0 or higher

Examples
============================
node ./dist/index "(hello world)"
node ./dist/index "cypress/fixtures/cliParameterParser-test-1.txt"
node ./dist/index "cypress/fixtures/cliParameterParser-test-2.txt"