import { LispValidator } from '../../dist/lib/lispValidator'

define('LISP Validator', ()=> {

    it('Should be valid string', () => {

        let lispValidator = new LispValidator();

        expect(lispValidator.isValid('(Hello World!!!)')).to.be.equal(true);
        expect(lispValidator.isValid(`(defun getPosInt ()
        (format t "Please enter a positive integer~%")
        (let ((N (read)))
             (if (isPosInt N) N
                (getPosint))))`)).to.be.equal(true);
        
    })

    it('Should be invalid string', () => {

        let lispValidator = new LispValidator();

        expect(lispValidator.isValid('(Hello World!!!')).to.be.equal(false);
        expect(lispValidator.isValid(`(defun getPosInt ()
        (format t "Please enter a positive integer~%")
        (let ((N read)))
             (if (isPosInt N N
                (getPosint)))`)).to.be.equal(false);
        expect(lispValidator.isValid(`)(defun getPosInt ()
        (format t "Please enter a positive integer~%")
        (let ((N read)))
             (if (isPosInt N N
                (getPosint)))`)).to.be.equal(false);
        
    })
})