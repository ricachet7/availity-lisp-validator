import { CLIParameterParser } from '../../dist/lib/cliParameterParser'

define('CLI Parameter Parser', ()=> {

    it('Parse should return input because it is not a valid file path', async () => {

        const data = '(Hello World!!!)';
        let cliParameterParser = new CLIParameterParser();

        expect(await cliParameterParser.parse(data)).to.be.equal(data);
    })

    it('Parse should return file content because input is a valid file path', async () => {

        const data = '../fixtures/cliParameterParser-test-1.txt';
        let cliParameterParser = new CLIParameterParser();

        cy.fixture('cliParameterParser-test-1.txt').then(async (dataFromFile) => {
          
            expect(await cliParameterParser.parse(data)).to.be.equal(dataFromFile);  
        })
    })
})